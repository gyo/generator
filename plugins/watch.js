const fs = require('fs')
const path = require('path')
const glob = require('tiny-glob')

module.exports = (config) => {
  config = Object.assign({

  }, {}, config)

  return {
    init() {
      if (this.mode !== 'development') return
      // eventType: 'rename' or 'change'
      fs.watch('./plugins', (eventType, filename) => {
        console.log(`plugin changed: ${eventType} ${filename}`)
      })
      fs.watch('./src', (eventType, filename) => {
        console.log(`src changed: ${eventType} ${filename}`)
      })
    },
    ready() {

    }
  }
}
