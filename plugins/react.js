const React = require('react')
const ReactDOMServer = require('react-dom/server')
import { printDrainHydrateMarks } from 'react-imported-component';

const renderMarkup = (state, html) => {
  return `<!DOCTYPE html>
<html>
  <head>
    <title>${state.page.title}</title>
    <meta charset="utf-8" />
  </head>
  <body>
    <div id="root">${html}</div>
    <script>window.__INITIAL_STATE__ = ${JSON.stringify(state)}</script>
    <!--<script src="/js/vendor.js"></script>-->
    <script src="/js/styles.js"></script>
    <script src="/js/app.js"></script>
  </body>
</html>`
}

module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    onRenderPage(page) {
      // if (!page[0].type || page[0].type !== 'react') return page
      const Component = require('../src/components/App').default
      const state = { route: { path: '/' + page.route }, page }
      page.content = renderMarkup(state, ReactDOMServer.renderToString(
        <Component {...state} />
      ) + printDrainHydrateMarks())
      return page
    }
  }
}
