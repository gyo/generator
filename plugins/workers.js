// The Cluster plugin allows to split the workload
// across several machines to improve the build speed
const { Worker } = require('worker_threads')

function WorkerPool(workerPath, numberOfThreads) {
  this.queue = []
  this.workersById = {}
  this.activeWorkersById = {}
  this.workerPath = workerPath
  this.numberOfThreads = numberOfThreads
  this.init()
}

WorkerPool.prototype.init = function() {
  if (this.numberOfThreads < 1) {
    return null
  }
  for (let i = 0; i < this.numberOfThreads; i += 1) {
    const worker = new Worker(this.workerPath)
    this.workersById[i] = worker
    this.activeWorkersById[i] = false
  }
}

WorkerPool.prototype.terminate = function() {
  Object.keys(this.workersById)
    .forEach(worker => this.workersById[worker].terminate())
}

WorkerPool.prototype.run = function(getData) {
  return new Promise((resolve, reject) => {
    const availableWorkerId = this.getInactiveWorkerId();
    const queueItem = {
      getData,
      callback: (error, result) => {
        if (error) {
          return reject(error)
        }
        return resolve(result)
      },
    };
   if (availableWorkerId === -1) {
      this.queue.push(queueItem);
      return null;
    }
    this.runWorker(availableWorkerId, queueItem);
  });
}

WorkerPool.prototype.getInactiveWorkerId = function() {
  for (let i = 0; i < this.numberOfThreads; i += 1) {
    if (!this.activeWorkersById[i]) {
      return i;
    }
  }
  return -1;
}

WorkerPool.prototype.runWorker = async function(workerId, queueItem) {
  const worker = this.workersById[workerId]
  this.activeWorkersById[workerId] = true
  const messageCallback = (result) => {
    queueItem.callback(null, result)
    cleanUp()
  }
  const errorCallback = (error) => {
    queueItem.callback(error)
    cleanUp()
  }
  const cleanUp = () => {
    worker.removeAllListeners('message')
    worker.removeAllListeners('error')
    this.activeWorkersById[workerId] = false
    if (!this.queue.length) {
      return null
    }
    this.runWorker(workerId, this.queue.shift())
  }
  worker.once('message', messageCallback)
  worker.once('error', errorCallback)
  worker.postMessage(await queueItem.getData())
}

module.exports = (config) => {
  config = Object.assign({
    worker: 'worker.js',
    batch: 1000,
    threads: require('os').cpus().length
  }, {}, config)
  let pool

  return {
    init() {
      pool = new WorkerPool(config.worker, config.threads)
      console.log('Workers initialized:', config.threads)
    },
    onRenderBatch(batch) {
      return pool.run(() => batch)
      // const batchSize = 2
      // const batchTotal = Math.ceil(batch.length / config.threads)
      // const tasks = []
      // for (let i = 1; i <= batchTotal; i++) {
      //   tasks.push(batch.slice((i - 1) * batch.length, i * batch.length))
      // }

      // return Promise.all(
      //   tasks.map(async pages => {
      //     return await pool.run(() => pages)
      //   })
      // )
    },
    ready() {
      pool.terminate()
    }
  }
}