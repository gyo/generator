/**
 * Add support for in-page DB queries
 * https://realm.io/docs/javascript/latest/api/tutorial-query-language.html
 *
 * This example will add the 'someKey' property to the page object
 * containing the query results (if any):
 * {
 *   ...
 *   query: [
 *      { k: 'someKey', q: 'id == "1"' }
 *   ]
 *   ...
 * }
 */
module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    init() {
      this.config.schema = this.config.schema.map(schema => {
        if (schema.name === 'Page' && !schema.properties.query) {
          schema.properties.query = 'string?[]'
        }
        return schema
      })
    },
    onRenderBatch(batch) {
      return batch.map(page => {
        if (page.query) {
          this.profiler.start('query')
          for (let i = 0; i < page.query.length; i++) {
            const query = JSON.parse(page.query[i])
            const pages = this.db.objects('Page').filtered(query.q)
            page[query.k] = pages[0].title
          }
          this.profiler.stop('query')
        }
        return page
      })
    },
    _beforeRenderPage(page) {
      if (this.isCluster) return page
      if (page.query) {
        this.profiler.start('query')
        for (let i = 0; i < page.query.length; i++) {
          const query = JSON.parse(page.query[i])
          const pages = this.db.objects('Page').filtered(query.q)
          page[query.k] = pages[0].title
        }
        page.query = null
        this.profiler.stop('query')
      }
      return page
    }
  }
}
