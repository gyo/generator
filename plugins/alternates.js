// Add alternate locales
exports.init = function() {
  // Extend the default 'Page' schema to add the 'alternates' property
  this.config.schema = this.config.schema.map(schema => {
    if (schema.name === 'Page' && !schema.properties.alternates) {
      schema.properties.alternates = 'string?[]'
    }
    return schema
  })
}

exports.onCreateNode = (page) => {
  return page
    .map((p, i) => {
      p.alternates = p.alternates || []
      page
        .filter((_, j) => i !== j)
        .forEach(p2 => p.alternates.push(p2.route))
      return p
    })
}