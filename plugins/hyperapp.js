// const hyperapp = require('hyperapp')
const hyperappRender = require('hyperapp-render')

module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    onCreateNode(page) {
      return page.map((p, i) => {
        const Component = require('../components/App').App
        p.content = hyperappRender.renderToString(Component({ todos: [], value: '', page: p }))
        return p
      })
    }
  }
}
