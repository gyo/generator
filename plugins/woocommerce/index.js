const fetch = require('node-fetch')
const mysql = require('mysql')
const unserialize = require('./unserialize')

// Dirty hack to allow invalid certificates (REMOVE IT!)
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0

const base64 = data => Buffer.from(data, 'utf8').toString('base64')

const f = (url, payload) => fetch(url, payload || {})
  .then(data => data.json())
  .catch(console.log)

const getPayload = (key, secret) => ({
  method: 'get',
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    'Authorization': 'Basic ' +  base64(key + ":" + secret)
  }
})

const WooCommerceAPI = ({ url, key, secret } ) => ({
  getProducts(page, pageSize) {
    console.log('getProducts', page)
    const qs = [`status=publish`, `page=${page}`, `per_page=${pageSize}`].join('&')
    console.log(`${url}/wp-json/wc/v3/products?${qs}`)
    return f(`${url}/wp-json/wc/v3/products?${qs}`, getPayload(key, secret))
  },
  getVariations(id) {
    const qs = [`status=publish`,`per_page=100`].join('&')
    return f(`${url}/wp-json/wc/v3/products/${id}/variations?${qs}`, getPayload(key, secret))
  }
})

const normaliseProduct = (product) => {
  return {
    id: product.id,
    type: product.type,
    price: product.price,
    regularPrice: product.regular_price,
    salePrice: product.sale_price,
    onSale: product.on_sale,
    purchasable: product.purchasable,
    attributes: product.type === 'variable'
      ? product.attributes.filter(attr => attr.visible && attr.variation)
      : product.attributes,
    manageStock: product.manage_stock,
    stockQuantity: product.stock_quantity,
    backorders: product.backorders,
  }
}

const findUniqueCombinations = (variations, attributes) => {
  return variations.filter((elem, index, self) => self.findIndex(
    (t) => {
      return attributes.every(attr => {
        const a1 = t.attributes.find(a => a.id === attr)
        const a2 = elem.attributes.find(a => a.id === attr)
        return a1 && a2 && a1.option === a2.option
      })
    }) === index)
}

const source = async function*(woo, pageSize = 10, attributes) {
  let page = 9; // base 1
  while (page < 10) {
    let data
    try {
      data = await woo.getProducts(page, pageSize)
    } catch (error) {
      console.log(error)
    }
    if (!data || !data.length) break
    for (let i = 0; i < data.length; i++) {
      // yield data[i]
      const meta = normaliseProduct(data[i])

      if (meta.type === 'variable') {
        const variations = await woo.getVariations(data[i].id) || []
        meta.variations = variations.map(normaliseProduct)
      }

      yield {
        id: data[i].id,
        route: data[i].slug,
        title: data[i].name,
        content: data[i].description,
        meta: JSON.stringify(meta)
      }

      if (meta.variations && meta.variations.length) {
        const variationPages = findUniqueCombinations(meta.variations, attributes)
        for (let j = 0; j < variationPages.length; j++) {
          console.log('variationPages[j]', variationPages[j]);
          yield {
            id: variationPages[j].id,
            route: variationPages[j].id + '-' + data[i].slug,
            title: variationPages[j].id + ' - ' + data[i].name,
            content: data[i].description,
            meta: JSON.stringify(variationPages[j])
          }
        }
      }
    }
    page++
  }
}

const productsSql = ({ page, pageSize, postType, postParent }) => {
  return `
  SELECT
    p.id,
    p.post_type AS type,
    p.post_title AS title,
    p.post_content AS content,
    p.post_name AS route,
    stock.meta_value AS stock,
    sku.meta_value AS sku,
    price.meta_value AS price,
    sale_price.meta_value AS salePrice,
    regular_price.meta_value AS regularPrice,
    product_attributes.meta_value AS attributes

  FROM wp_posts p

  INNER JOIN wp_postmeta stock ON (p.id = stock.post_id)
  INNER JOIN wp_postmeta sku ON (p.id = sku.post_id)
  INNER JOIN wp_postmeta price ON (p.id = price.post_id)
  INNER JOIN wp_postmeta sale_price ON (p.id = sale_price.post_id)
  INNER JOIN wp_postmeta regular_price ON (p.id = regular_price.post_id)
  INNER JOIN wp_postmeta product_attributes ON (p.id = product_attributes.post_id)

  WHERE p.post_type = '${postType || 'post'}'
  ${postParent ? ` AND p.post_parent = ${postParent} ` : ''}
  AND stock.meta_key = '_stock'
  AND sku.meta_key = '_sku'
  AND price.meta_key = '_price'
  AND sale_price.meta_key = '_sale_price'
  AND regular_price.meta_key = '_regular_price'
  AND product_attributes.meta_key = '_product_attributes'

  LIMIT ${pageSize} OFFSET ${page}`
}

/*
  TODO:
    Return a promise and resolve it inside the connection.connect() callback:

    return new Promise((resolve, reject) => {
      connection.connect(function(err) {
        if (err) {
          reject(err)
          return
        }
        resolve({
          query,
          getProducts,
          getVariations,
          stream
        })
      })
    })
*/
const WP = config => {
  const connection = mysql.createConnection({
    host: config.dbHost,
    port: config.dbPort,
    user: config.dbUser,
    password: config.dbPass,
    database: config.dbName
  })

  connection.connect()

  const query = sql => new Promise(resolve => {
    console.log('query....', connection);
    connection.query(sql, (error, results, fields) => {
      console.log('results', results);
      if (error) throw error
      resolve(results)
    })
  })

  const getProducts = (page, pageSize) => query(productsSql({
    page,
    pageSize,
    postType: 'product'
  }))

  const getVariations = id => query(productsSql({
    page,
    pageSize,
    postType: 'product_variation',
    postParent: id
  }))

  const normaliseProduct = product => ({
    id: product.id,
    route: product.route,
    title: product.title,
    content: product.content,
    meta: JSON.stringify({
      stock: product.stock,
      sku: product.sku,
      price: product.price,
      salePrice: product.salePrice,
      regularPrice: product.regularPrice,
      attributes: unserialize(product.attributes)
    })
  })

  const stream = async function*(page, pageSize) {
    let currentPage = page; // base 0
    while (currentPage < 3) { // TODO: Get total pages
      console.log('Getting...', currentPage, '/ 3')
      let data
      try {
        data = await getProducts(currentPage, pageSize)
      } catch (error) {
        console.log(error)
      }
      if (!data || !data.length) break
      for (let i = 0; i < data.length; i++) {
        yield normaliseProduct(data[i])
      }
      currentPage++
    }

    connection.end()
  }

  return {
    query,
    getProducts,
    getVariations,
    stream
  }
}

module.exports = (config) => {
  config = Object.assign({
    url: '',
    key: '',
    secret: '',
    pageSize: 100,
    attributes: [39] // Variation attributes ID to generate single pages for
  }, config)

  return {
    async *createNodes() {
      // const woo = WooCommerceAPI({
      //   url: config.url,
      //   key: config.key,
      //   secret: config.secret
      // })
      // for await (let page of source(woo, config.pageSize, config.attributes)) {
      //   // await this.createNode(page)
      //   // console.log(product)
      //   yield page
      // }

      const wp = WP({
        dbHost: 'localhost',
        dbPort: 10010,
        dbUser: 'root',
        dbPassword: '',
        dbName: 'gaco'
      })

      for await (let page of wp.stream(0, config.pageSize)) {
        // await this.createNode(page)
        console.log('product', page)
        yield page
      }

      // const sql = productsSql(0, config.pageSize)

      // const results = await query(connection, sql)
      // results.forEach(result => console.log(result.id, ':', result.title))

      // connection.end()
    },
    async _createNodes() {
      const api = WooCommerceAPI(config.url, config.key, config.secret)
      const collections = config.collections && config.collections.length || await api.getCollections()
      const singletons = config.singletons && config.singletons.length || await api.getSingletons()

      // await createNodes.apply(this, [api, config, collections, singletons])

      for await (let collection of collections) {
        if (config.mapping[collection]) {
          const pages = await api.getCollection(collection, { limit: 1 })

          for await (let entry of pages.entries) {
            const page = []
            this.config.locales.forEach(locale => {
              const p = { locale: locale.id }
              Object.keys(config.mapping[collection]).forEach(field => {
                const key = config.mapping[collection][field]
                const localizedKey = config.mapping[collection][field] + '_' + locale.id

                p[field] = entry[localizedKey] || entry[key]
                // const key = config.mapping[collection][field]
                // const localizedFieldKey = pages.fields[key] && pages.fields[key].localize
                //   ? locale.id === this.config.defaultLocale ? field : '_' + locale.id
                //   : field
                // p[field] = entry[config.mapping[collection][field]]
              })
              page.push(p)
            })
            // console.log('Cockpit page:', page)
            await this.createNode(page)
          }
        }
      }

      // console.log('Singletons', collections)
      // singletons.forEach(async singleton =>
      //   console.log(singleton, await api.getSingleton(singleton, { limit: 1 })))
    }
  }
}
