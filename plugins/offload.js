const remote = batch => new Promise(resolve => setTimeout(resolve(batch), 1000))

module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    async onRenderBatch(batch) {
      // Offload the batch to a remote machine
      batch = await remote(batch)
      // ...
      // for (let i = 0; i < batch.length; i++) {
      //   await this.renderPage(batch[i]);
      // }
      batch.isRendered = true
      return Promise.all(batch.map(page => this.renderPage(page, true)))
      // return batch
    }
  }
}
