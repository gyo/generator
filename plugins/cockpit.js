const fetch = require('node-fetch')

const f = (url, payload) => fetch(url, payload || {}).then(data => data.json())
// /api/collections/get/Products?token=xxx&filter[_modified][$gte]=1533752348
const CockpitAPI = (url, token) => ({
  getCollections(payload) {
    return f(`${url}/api/collections/listCollections?token=${token}`, payload || null)
  },
  getCollection(collection, payloadBody) {
    const payload = {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(Object.assign({
        // filter: {
        //   _modified: {
        //     $gte: '1598026137418'
        //   }
        //   // published: true
        // },
        // fields: { fieldA: 1, fieldB: 1},
        // limit: 1,
        // skip: 5,
        // sort: {_created:-1},
        // populate: 1, // resolve linked collection items
        // lang: 'de' // return normalized language fields (fieldA_de => fieldA)
      }, payloadBody || {}))
    }
    console.log(`${url}/api/collections/get/${collection}?token=${token}`, payloadBody)
    return f(`${url}/api/collections/get/${collection}?token=${token}`, payloadBody ? payload : null)
  },
  getSingletons() {
    return f(`${url}/api/singletons/listSingletons?token=${token}`)
  },
  getSingleton(singleton) {
    return f(`${url}/api/singletons/get/${singleton}?token=${token}`)
  }
})

module.exports = (config) => {
  config = Object.assign({
    url: 'http://localhost/cockpit',
    token: '',
    collections: null, // array or null
    singletons: null, // array or null
    pages: { collections: ['Products'], singletons: [] },
    mapping: {
      'Products': {
        id: '_id',
        route: 'slug',
        title: 'title',
        content: 'body',
      }
    }
  }, config)

  return {
    async *createNodes() {
      const api = CockpitAPI(config.url, config.token)
      const collections = config.collections && config.collections.length || await api.getCollections()
      const singletons = config.singletons && config.singletons.length || await api.getSingletons()

      // await createNodes.apply(this, [api, config, collections, singletons])

      for (let collection of collections) {
        if (config.mapping[collection]) {
          const pages = await api.getCollection(collection, { limit: 0 })

          for (let entry of pages.entries) {
            const page = []
            this.config.locales.forEach(locale => {
              const p = { locale: locale.id }
              Object.keys(config.mapping[collection]).forEach(field => {
                const key = config.mapping[collection][field]
                const localizedKey = config.mapping[collection][field] + '_' + locale.id

                p[field] = entry[localizedKey] || entry[key]
                // const key = config.mapping[collection][field]
                // const localizedFieldKey = pages.fields[key] && pages.fields[key].localize
                //   ? locale.id === this.config.defaultLocale ? field : '_' + locale.id
                //   : field
                // p[field] = entry[config.mapping[collection][field]]
              })
              page.push(p)
            })
            // console.log('Cockpit page:', page)
            // await this.createNode(page)
            yield page
          }
        }
      }

      // console.log('Singletons', collections)
      // singletons.forEach(async singleton =>
      //   console.log(singleton, await api.getSingleton(singleton, { limit: 1 })))
    }
  }
}
