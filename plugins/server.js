const path = require('path')
const webpack = require('webpack')
// const ProgressPlugin = require('webpack/lib/ProgressPlugin')
const commonPaths = require("../build-tools/common-paths")
// const webpackConfig = require('../webpack.config')('dev')

const express = require('express')
const app = express()
// const filePath = path.join(__dirname, './public/js/')
// const fileName = 'bundle.js'

const WebpackReporter = new webpack.ProgressPlugin((percentage, message) => {
  process.stdout.clearLine()
  process.stdout.cursorTo(0)
  process.stdout.write(`Webpack bundling... ${(percentage * 100).toFixed()}% ${message}`)
})

const webpackConfig = {
  mode: 'development',
  stats: 'none',
  entry: {
    app: [
      `${commonPaths.appEntry}/index.js`,
      `webpack-hot-middleware/client?path=/__webpack_hmr&reload=true`,
    ],
  },

  output: {
    path: `${commonPaths.outputPath}/js`,
    publicPath: `${commonPaths.outputPath}/js/`,
    filename: '[name].js',
    hotUpdateChunkFilename: '.hot/hot-update.js',
    hotUpdateMainFilename: '.hot/hot-update.json',
  },

  watchOptions: {
    ignored: '/node_modules/',
  },

  resolve: {
    extensions: [
      '.js','.jsx'
    ]
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
              '@babel/preset-react'
            ],
            plugins: [
              // ['@babel/plugin-proposal-decorators', { "legacy": true }],
            ]
          },
        }
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    WebpackReporter,
  ]
}

module.exports = (config) => {
  config = Object.assign({
    port: 3000
  }, {}, config)

  return {
    ready() {
      const webpackCompiler = webpack(webpackConfig)

      // Monitor build process (run/watch)
      webpackCompiler.hooks.done.tapAsync('GeneratorServer', (params, callback) => {
        process.stdout.write('  DONE!')
        process.stdout.write("\n")
        callback()
      })

      if (this.config.mode === 'development') {
        // Express configuration
        app.disable('x-powered-by')

        app.use(require('webpack-dev-middleware')(webpackCompiler, {
          noInfo: true,
          publicPath: webpackConfig.output.publicPath,
          stats: 'errors-only',
          state: false,
          writeToDisk: true,
        }))

        app.use(require('webpack-hot-middleware')(webpackCompiler, {
          noInfo: true,
          path: '/__webpack_hmr',
          stats: 'errors-only',
        }))

        app.use(express.static(commonPaths.outputPath))

        // Routes
        // app.get('/', (req, res) => {
        //   res.sendFile(`${commonPaths.outputPath}/index.html`)
        // })

        app.get('*', async (req, res) => {
          // const output = []
          let route = req.path.replace(/^\/|\/$/g, '').replace('/page-data.json', '')
          // route = route === '' || route === '/' ? 'index' : route
          // console.log('route', route)
          const pages = this.db.objects('Page')
            .filtered('route = $0', route)
          const page = pages[0]

          // for (let i = 0; i < pages.length; i++) {
          //   output.push(pages[i])
          // }
          // console.log('path:', req.path, 'clean path:', req.path.replace(/^\/|\/$/g, ''))
          // res.json(output[0])
          if (page) {
            // res.send(page.content)

            // TODO: remove this
            let p = { component: 'Page' }
            this.config.schema.forEach(schema => {
              if (schema.name === 'Page') {
                Object.keys(schema.properties).forEach(property => {
                  p[property] = page[property]
                })
              }
            })

            if (req.path.endsWith('page-data.json')) {
              res.json(p)
            } else {
              const output = await this.renderPage(p, false)
              res.set('Content-Type', 'text/html')
              res.send(output.content)
            }
            return
          }

          res.sendStatus(404)
        })

        app.listen(config.port, '0.0.0.0', () => {
          console.log(`Dev-Server is listening on port ${config.port}`)
        })
      }
    }
  }
}
