module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    async renderPages() {
      const pages = this.db.objects('Page')
        .filtered('title = $0 OR title = $1 OR title = $2', 'About', 'Nosotros', 'Noi')
      const content = [
        pages[0] ? pages[0].title : 'no pages found for the archive',
        pages[1] ? pages[1].title : 'no pages found for the archive',
      ]
      await this.createNode({
        id: 'archive1',
        route: 'about-archive',
        locale: 'en',
        title: 'About Archive',
        content: JSON.stringify(content),
        // skipIndexing: true,
      }, true)
    }
  }
}
