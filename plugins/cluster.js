// The Cluster plugin allows to split the workload
// across several machines to improve the build speed
const fetch = require('node-fetch')
const express = require('express')
const app = express()

module.exports = (config) => {
  config = Object.assign({
    port: 3001,
    batch: 5000,
    isCluster: false,
    clusters: ['http://192.168.1.137:3001']
  }, {}, config)
  let currentCluster = -1

  return {
    init() {
      // Initialise the server
      if (config.isCluster) {
        app.use(express.json({ limit: '50mb' }))

        // Catch-all fallback
        app.get('*', async (req, res) => {
          console.log(req)
          res.set('Content-Type', 'text/html')
          res.send('OK')
        })

        // Render pages coming from a remote machine
        // Send back a post request with the rendered pages as a json array
        app.post('*', async (req, res) => {
          if (req.body && req.body.length) {
            for (let i = 0; i < req.body.length; i++) {
              req.body[i] = await this.renderPage(req.body[i], false)
            }
          }
          console.log('Cluster processed the request!', req.body.map(x => x.route))
          res.set('Content-Type', 'application/json')
          res.json(req.body)
        })

        app.listen(config.port, '0.0.0.0', () => {
          console.log(`Cluster listening on port ${config.port}`)
        })
      }
    },
    async onRenderBatch(batch) {
      let result
      // Process the first batch on the local computer
      if (currentCluster === -1) {
        result = batch
        // console.log('Local computer processing', batch.length, 'pages')
        // for (let j = 0; j < batch.length; j++) {
        //   await this.renderPage(batch[j]);
        // }
      // Process the next batch on a remote cluster
      } else if (config.clusters[currentCluster]) {
        // Send the batch to the cluster
        const cluster = config.clusters[currentCluster]
        const payload = {
          method: 'post',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(batch)
        }
        console.log('Cluster processing', batch.length, 'pages')

        // const response = await fetch(cluster, payload)
        // // Receive the rendered pages
        // if (response) {
        //   const pages = await response.json()
        //   console.log('Cluster sent', pages.length, 'rendered pages')
        //   // console.log('Response from Cluster!', pages.map(page => page.route))
        //   for (let i = 0; i < pages.length; i++) {
        //     await this.write(pages[i])
        //   }
        // }

        result = new Promise(async resolve => {
          let response
          try {
            response = await fetch(cluster, payload)
            response = await response.json()
            if (response) response.isRendered = true
          } catch (e) {
            console.log('Error contacting the remote cluster, the batch will be processed by the local machine.', e)
          }
          resolve(response || batch)
        })
      }

      // Increase the cluster index or reset to -1 (local machine)
      currentCluster = config.clusters[currentCluster + 1] ? currentCluster + 1 : -1

      return result
    }
  }
}