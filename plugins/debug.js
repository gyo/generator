module.exports = (config) => {
  config = Object.assign({}, {}, config)
  let pagesBuilt = 0

  return {
    onWrite(page) {
      pagesBuilt++
      return page
    },
    ready() {
      let pages = this.db.objects('Page')
      // let array = Array.from(pages)
      console.log('Pages:', pages.length)
      console.log('Pages written to disk:', pagesBuilt)
      // for (let i = 0; i < pages.length; i++) {
      //   console.log(pages[i].id, pages[i].route, pages[i].alternates)
      // }

      // Create page after the main build has run
      // setTimeout(async () => {
      //   const page = [
      //     {
      //       id: 'xyz',
      //       component: 'Page',
      //       route: 'xyz',
      //       locale: 'en',
      //       layout: 'page',
      //       title: 'XYZ!!!',
      //       content: 'XYZ!!!...'
      //     },
      //     {
      //       route: 'xyz-es',
      //       locale: 'es',
      //       title: 'XYZ ES!!!',
      //       content: 'XYZ ES!!!...'
      //     }
      //   ]
      //   await this.createNode(page)
      // }, 2000);
    }
  }
}
