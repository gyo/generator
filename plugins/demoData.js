// An example plugin to create pages from some API

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

// Test data
const pages = [
  [
    {
      id: '1',
      route: 'index',
      locale: 'en',
      layout: 'home',
      title: 'Home',
      content: 'This is the home',
      query: [
        JSON.stringify({ k: 'query_translationES', q: 'id == "1_es" && locale == "es"' })
      ]
    },
    {
      locale: 'es',
      title: 'Inicio',
      content: 'Esta es la home'
    },
    {
      locale: 'it',
      title: 'Inizio',
      content: 'Questa é la home'
    },
  ],
  [
    {
      id: '2',
      route: 'about',
      locale: 'en',
      layout: 'page',
      title: 'About',
      content: 'A fancy about page',
      query: [
        JSON.stringify({ k: 'query_translationES', q: 'id == "1_es" && locale == "es"' })
      ]
    },
    {
      route: 'nosotros',
      locale: 'es',
      title: 'Nosotros',
      content: 'Algo sobre nosotros'
    },
    {
      route: 'noi',
      locale: 'it',
      title: 'Noi',
      content: 'Qualcosa su di noi'
    },
  ],
  [
    {
      id: '3',
      component: 'Page',
      route: 'somepage',
      locale: 'en',
      layout: 'page',
      title: 'Some Page',
      content: 'Some page...'
    },
    {
      route: 'algunapagina',
      locale: 'es',
      title: 'Alguna Página',
      content: 'Alguna página...'
    },
    {
      route: 'unapagina',
      locale: 'it',
      title: 'Una Pagina',
      content: 'Una pagina...'
    },
  ],
]

const api = async (page, pageSize) => {
  // await sleep(1000)
  // const limit = pageSize
  // const skip = (page - 1) * pageSize
  // The pages should come from an API request
  return pages.slice((page - 1) * pageSize, page * pageSize);
}

const arraySource = async function*(pages) {
  for await (let page of pages) {
    // await sleep(500)
    yield page
  }
}

const bigSource = async function*(total) {
  var i = 0;
  while (i < total) {
    const id = 'dyn' + i
    const page = [
      {
        id: id,
        route: 'page-' + id,
        locale: 'en',
        title: 'Page ' + id,
        content: 'This is the page ' + id
      },
      {
        locale: 'es',
        title: 'Pagina ' + id,
        content: 'Esta es la pagina ' + id
      },
      {
        locale: 'it',
        title: 'Pagina ' + id,
        content: 'Questa é la pagina ' + id
      },
    ]

    yield page
    i++
  }
}

module.exports = (config) => {
  config = Object.assign({
    totalPages: 10
  }, {}, config)

  return {
    async *createNodes() {
      const homepage = [
        {
          id: 1,
          route: 'index',
          locale: 'en',
          layout: 'home',
          title: 'Home',
          content: 'This is the home'
        },
        {
          locale: 'es',
          title: 'Inicio',
          content: 'Esta es la home'
        },
        {
          locale: 'it',
          title: 'Inizio',
          content: 'Questa é la home'
        },
      ]

      yield homepage
      
      // Paginate results from fake api
      // const pageSize = 2
      // let index = 1;
      // while (index > -1) {
      //   const data = await api(index, pageSize)
      //   if (!data || !data.length) break
      //   for (let i = 0; i < data.length; i++) {
      //     yield data[i]
      //   }
      //   index++
      // }

      // for await (let page of pages) {
      //   await this.createNode(page)
      // }

      if (config.totalPages > 0) {
        for await (let page of bigSource(config.totalPages)) {
          // await this.createNode(page)
          // console.log(page)
          yield page
        }
      }

      // for await (let page of arraySource(pages)) {
      //   await this.createNode(page)
      // }

      // pages.forEach(page => {
      //   this.createNode(page)
      // })
    },

  }
}
