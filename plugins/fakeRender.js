const renderMarkup = (html) => {
  return `<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta charset="utf-8" />
  </head>
  <body>
    <div id="root">${html}</div>
  </body>
</html>`
}

module.exports = (config) => {
  config = Object.assign({}, {}, config)

  return {
    onRenderPage(page) {
      page.content = renderMarkup(page.content)
      return page
    }
  }
}
