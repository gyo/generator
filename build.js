// import path from 'path'
// import Generator from './generator'
// import demoData from './plugins/demoData'
// import plugin2 from './plugins/plugin2'
// import react from './plugins/react'
// import hyperapp from './plugins/hyperapp'

const path = require('path')
const Generator = require('./generator')
const config = require('./config')
const cockpit = require('./plugins/cockpit')
const demoData = require('./plugins/demoData')
const alternates = require('./plugins/alternates')
const react = require('./plugins/react')
const server = require('./plugins/server')
const debug = require('./plugins/debug')
const archives = require('./plugins/archives')
const query = require('./plugins/query')
const watch = require('./plugins/watch')
const cluster = require('./plugins/cluster')
const workers = require('./plugins/workers')
// const hyperapp = require('./plugins/hyperapp')
// const woocommerce = require('./plugins/woocommerce')

const generator = new Generator(config)

// generator.use(cockpit({
//   url: 'https://admin.madrelievito.com',
//   token: '2bc9614504c2c5d01f931dd9d1c6c7'
// }))
// generator.use(woocommerce({
//   url: 'https://gaco.test',
//   key: 'ck_f0dd825b18994f2a7e110571262bde30861f1e5d',
//   secret: 'cs_b75924865c9ac31a093c4e63c58c99d15f297aad'
// }))
generator.use(demoData({ totalPages: 10 }))
// generator.use(query())
// generator.use(workers())
// generator.use(alternates)
// generator.use(workers({
//   worker: './worker.js'
// }))
// generator.use(cluster())
generator.use(react())
// generator.use(archives())
// generator.use(hyperapp())
// generator.use(watch())
generator.use(debug())
generator.use(server())
generator.build()
