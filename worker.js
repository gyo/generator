require("@babel/register")

// const Generator = require('./generator')
// const schema = require('./schema')
// const alternates = require('./plugins/alternates')
// const react = require('./plugins/react')
// const query = require('./plugins/query')

// const generator = new Generator({
//   output: 'build',
//   schema: [schema.Page],
//   defaultLocale: 'en',
//   locales: [
//     {
//       id: 'en',
//       iso: 'en-US',
//     },
//     {
//       id: 'es',
//       iso: 'es-ES',
//     },
//     // {
//     //   id: 'it',
//     //   iso: 'it-IT',
//     // },
//   ]
// })

// // generator.use(plugin1())
// generator.use(query())
// generator.use(alternates)
// generator.use(react())

const { workerData, parentPort, isMainThread } = require("worker_threads");

// Plugins to run
const plugins = [
  // require('./plugins/query')(),
  require('./plugins/react')(),
  // require('./plugins/fakeRender')(),
]

// Version 2
// Use events if you send data through worker.postMessage
parentPort.on('message', async (pages) => {
  for (let i = 0; i < pages.length; i++) {
    // pages[i] = await generator.hook('beforeRenderPage', [pages[i]])
    // pages[i] = await generator.hook('onRenderPage', [pages[i]])

    // for (let j = 0; j < plugins.length; j++) {
    //   pages[i] = plugins[j].beforeRenderPage
    //     ? await plugins[j].beforeRenderPage(pages[i])
    //     : pages[i]
    // }
    for (let j = 0; j < plugins.length; j++) {
      pages[i] = plugins[j].onRenderPage
        ? await plugins[j].onRenderPage(pages[i])
        : pages[i]
    }
  }
  parentPort.postMessage(pages)
});

// In the browser
// self.addEventListener('message', function(e) {
//   e.data = e.data || []
//   const pages = []
//   for (let i = 0; i < e.data.length; i++) {
//     let page = e.data[i]
//     for (let j = 0; j < plugins.length; j++) {
//       page = plugins[j].onRenderPage
//         ? plugins[j].onRenderPage(page)
//         : page
//     }
//     pages.push(page)
//   }
//   self.postMessage(pages)
//   self.close()
// })

// console.log('Worker data', workerData)

// async function process() {
//   console.log('Worker received workerData:', workerData)
//   const { pages } = workerData
//   const newPages = []

//   // pages = pages.map(page => {
//   //   for (let j = 0; j < plugins.length; j++) {
//   //     page = plugins[j].onRenderPage
//   //       ? plugins[j].onRenderPage(page)
//   //       : page
//   //   }
//   //   return page
//   // })

//   console.log('Worker rendering', pages.length, 'pages...')
//   for (let i = 0; i < pages.length; i++) {
//     let page = pages[i]
//     for (let j = 0; j < plugins.length; j++) {
//       page = plugins[j].beforeRenderPage
//         ? await plugins[j].beforeRenderPage(page)
//         : page
//     }
//     for (let j = 0; j < plugins.length; j++) {
//       page = plugins[j].onRenderPage
//         ? await plugins[j].onRenderPage(page)
//         : page
//     }
//     newPages.push(page)
//   }

//   parentPort.postMessage(newPages)
// }
// process()