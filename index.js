// Get CLI params
const cli = (param) => {
  const paramIndex = process.argv.findIndex(arg => arg === param)
  if (paramIndex < 0) return
  // const paramName = process.argv[paramIndex]
  const paramValue = process.argv[paramIndex + 1]
  return paramValue
}

const build = cli('run') || 'build'

require('@babel/register')
require('./' + build)
