const commonPaths = require("./common-paths");

const webpack = require("webpack");

const port = process.env.PORT || 3000;

const config = {
  mode: "development",
  entry: {
    app: [`${commonPaths.appEntry}/index.js`],
  },
  output: {
    path: `${commonPaths.outputPath}/js`,
    filename: "[name].js",
    publicPath: `${commonPaths.outputPath}/js/`,
    hotUpdateChunkFilename: '.hot/hot-update.js',
    hotUpdateMainFilename: '.hot/hot-update.json',
    // filename: "[name].[hash].js",
  },
  resolve: {
    alias: {
      "react-dom": "@hot-loader/react-dom",
    },
  },
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              localsConvention: "camelCase",
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
  watchOptions: {
    ignored: '/node_modules/',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: commonPaths.outputPath,
    publicPath: '/',
    host: 'localhost',
    port: port,
    historyApiFallback: true,
    hot: true,
    open: false,
    writeToDisk: true,
  },
};

module.exports = config;
