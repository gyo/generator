const test = require('tape')
const Generator = require('../generator')
const demoData = require('../plugins/demoData')
const schema = require('../schema')

test('build', function (t) {
  const generator = new Generator({
    output: 'temp/build',
    schema: [schema.Page],
    plugins: [
      demoData(12)
    ]
  })

  generator.build()

  t.equal(typeof Date.now, 'function')
  t.end()
})