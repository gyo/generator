require("@babel/register")
const fs = require('fs')
const path = require('path')

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

// Workers test
// const worker = new Worker('worker_createPages.js')
// worker.addEventListener('message', (e) => {
//   console.log(e.data)
//   e.data.forEach(p => this.write(p))
// })
// worker.postMessage(page)

// Recursive directory walk generator
async function* walk(dir) {
  for await (const d of await fs.promises.opendir(dir)) {
    const entry = path.join(dir, d.name)
    if (d.isDirectory()) yield* await walk(entry)
    else if (d.isFile()) yield entry
  }
}

async function main() {
  // for await (const p of walk('build/'))
  //   console.log(p)

  const w = walk('build/')
  console.log(await w.next())
  console.log(await w.next())
  console.log(await w.next())
  console.log(await w.next())
  console.log(await w.next())
  console.log(await w.next())
  console.log(await w.next())
}

// main()

//////////////////////////////////////////////

// Async generator streams

var source = async function*() {
  var i = 0;
  while (true) {
    await sleep(Math.random() * 1000);
    yield i++;
  }
};

var run = async function(stream) {
  // var stream = source();
  for await (let n of stream) {
    console.log(n);
  }
};

// run(source());

//////////////////////////////////////////////

// Realm DB test

const Realm = require('realm')
const schema = require('./schema')

function realmTest() {
  let schemaVersion = Realm.schemaVersion('temp/db.realm')
  let realm = new Realm({
    path: 'temp/db.realm',
    // path: 'temp/test.realm',
    schema: [schema.Page],
    schemaVersion: schemaVersion + 1
  })





  // const schemas = [
  //   { schema: schema1, schemaVersion: 1, migration: migrationFunction1 },
  //   { schema: schema2, schemaVersion: 2, migration: migrationFunction2 },
  // ]

  // // The first schema to update to is the current schema version
  // // since the first schema in our array is at nextSchemaIndex:
  // let nextSchemaIndex = Realm.schemaVersion(Realm.defaultPath);

  // // If Realm.schemaVersion() returned -1, it means this is a new Realm file
  // // so no migration is needed.
  // if (nextSchemaIndex !== -1) {
  //   while (nextSchemaIndex < schemas.length) {
  //     const migratedRealm = new Realm(schemas[nextSchemaIndex++]);
  //     migratedRealm.close();
  //   }
  // }

  // // Open the Realm with the latest schema
  // Realm.open(schemas[schemas.length-1]);






  // realm.write(() => {
  //   realm.create('Page', {
  //     route: 'test',
  //     locale: 'en',
  //     title: 'Some title',
  //     content: 'Some content',
  //   }, true)
  // })

  let pages = realm.objects('Page')
  // let array = Array.from(pages)
  console.log('Indexed Pages:', pages.length)
  for (let i = 0; i < pages.length; i++) {
    console.log(pages[i].route, pages[i].alternates)
  }

  process.exit()


  // const pages = this.db.objects('Page')

  pages.addListener((collection, changes) => {
    // collection === pages
    if (changes.insertions.length || changes.modifications.length || changes.deletions.length) {
      console.log('===== DB UPDATE =====')
      console.log(`${changes.insertions.length} insertions`)
      console.log(`${changes.modifications.length} modifications`)
      console.log(`${changes.deletions.length} deletions`)
      console.log(`New size of collection: ${collection.length}`)
    }
  })

  //
  setTimeout(() => {
    const page = [
      {
        id: 4,
        route: 'about2',
        locale: 'en',
        layout: 'page',
        title: 'About!!',
        content: 'A fancy about page!!'
      },
      {
        route: 'nosotros2',
        locale: 'es',
        title: 'Nosotros!',
        content: 'Algo sobre nosotros!!'
      },
      {
        route: 'noi2',
        locale: 'it',
        title: 'Noi!',
        content: 'Qualcosa su di noi!!'
      },
    ]
    this.createNode(page).then(() => {
      this.processIndexQueue()
    })
    console.log('page created')
  }, 5000)
}

// realmTest()

//////////////////////////////////////////////


const Profiler = () => {
  const profiles = {}
  return {
    start(profile) {
      profiles[profile] = profiles[profile] || { start: 0, total: 0 }
      profiles[profile].start = Date.now()
    },
    stop(profile) {
      if (profiles[profile]) {
        profiles[profile].total += Date.now() - profiles[profile].start
        profiles[profile].start = 0
      }
    },
    stats() {
      return profiles
    }
  }
}

// const profiler = Profiler()
// profiler.start('A')
// setTimeout(() => {
//   profiler.stop('A')
//   console.log(profiler.stats())
// }, 1000);

//////////////////////////////////////////////

// fake fetch for demo purposes only
const fakeFetch = (data) => new Promise(resolve => setTimeout(resolve, 300, data))

// task executor
const addTask = (() => {
  let pending = Promise.resolve()

  const run = async (data) => {
    try {
      await pending
    } finally {
      return fakeFetch(data)
    }
  }

  // update pending promise so that next task could await for it
  return (data) => (pending = run(data))
})();

// addTask('url1').then(console.log)

// addTask('url2').then(console.log)

// setTimeout(() => {
//   addTask('url3').then(console.log)
// }, 2000)

//////////////////////////////////////////////


// Simple batch queue with timeout support
function Queue(opts) {
  const config = Object.assign({}, {
    timeout: 100,
    batch: 10,
    process: items => items
  }, opts)

  const tasks = []
  let timer = null

  const push = item => {
    clearTimeout(timer)
    tasks.push(item)
    if (tasks.length >= config.batch) {
      process()
    } else {
      timer = setTimeout(process, config.timeout)
    }
  }

  const process = () => {
    config.process(tasks.splice(tasks.length - config.batch, config.batch))
    if (tasks.length) process() // keep processing if necessary
  }

  return { push, process, tasks }
}

const q = Queue({
  timeout: 1000,
  batch: 5,
  process: items => {
    console.log('Processing batch of', items, 'items')
  }
})

// for (let i = 1; i <= 11; i++) {
//   q.push(i)
// }


const ConcurrentQueueOriginal = (concurrency) => {
  let running = 0
  const tasks = []

  return async (task) => {
    tasks.push(task)
    if (running >= concurrency) return

    ++running
    while (tasks.length) {
      try {
        await tasks.shift()()
      } catch(err) {
        console.error(err)
      }
    }
    --running
  }
}

// const e = ConcurrentQueueOriginal(2)

const ConcurrentQueue = (concurrency, done) => {
  let running = 0
  const tasks = []

  return async task => {
    tasks.push(task)
    if (running >= concurrency) return

    ++running
    while (tasks.length) {
      try {
        await tasks.shift()()
      } catch(err) {
        console.error(err)
      }
    }
    --running

    if (!tasks.length && !running) {
      done && done()
    }
  }
}

// Create the queue with a concurrency limit
// const concurrency = 100
// const enqueue = ConcurrentQueue(concurrency, () => {
//   console.log('Queue done')
// })

// ;(async () => {
//   for (let i = 0; i < 100; i++) {
//     console.log('Added to queue:', i)
//     enqueue(async () => {
//       console.log('Running:', i)
//       await sleep(1000)
//     })
//   }
// })();

// const items = [1,2,3]
// items.forEach((item) => {
// for (let i = 0; i < 1000; i++) {
//   console.log('Added to queue:', i)
//   enqueue(async () => {
//     console.log('Running:', i)
//     await sleep(1000)
//   })
// }

// const wait = async () => {

// }

// console.log('Done!!!!!')

// setTimeout(() => {
//   enqueue(async () => {
//     console.log(4)
//     return sleep(100)
//   })
// }, 1000);



// ;(async () => {
//   let done = false;

//   setTimeout(() => {
//     done = true
//   }, 5);

//   const eventLoopQueue = () => {
//     return new Promise(resolve =>
//       setImmediate(() => {
//         console.log('event loop');
//         resolve();
//       })
//     );
//   }

//   const wait = async () => {
//     while (!done) {
//       console.log('loop');
//       // await eventLoopQueue();
//       setImmediate(() => {})
//     }
//   }

//   // wait().then(() => console.log('Done'));
//   await wait()
//   console.log('Done')
// })();



//////////////////////////////////////////////////

// const {
//   performance,
//   PerformanceObserver
// } = require('perf_hooks');

// const obs = new PerformanceObserver((list, observer) => {
//   console.log(list.getEntries());
//   observer.disconnect();
// });
// obs.observe({ entryTypes: ['measure'], buffered: true });

// performance.mark('test1');
// performance.mark('test2');
// performance.measure('test', 'test1', 'test2')


///////////////////////////////////////////////////////

const { Worker } = require('worker_threads')

function process(pages) {
  return new Promise((resolve, reject) => {
    const worker = new Worker(__dirname + "/worker.js", {
      workerData: { pages }
    })
    worker.on("message", resolve);
    worker.on("error", reject);
    worker.on("exit", code  => {
      if (code  !==  0)
        reject(new  Error(`Worker stopped with exit code ${code}`));
    });
  });
};

// const pages = [{id:'1', route:'index', content: 'Home'}, {id:'2', route:'about', content: 'About'}]
// // process(pages).then(result => {
// //   console.log('Received result from worker')
// //   console.log(result)
// // }).catch(console.error)

// const worker = new Worker(__dirname + "/worker.js")
// worker.on("message", console.log)
// // worker.on("error", reject);
// worker.on("exit", code  => {
//   if (code  !==  0)
//     console.error(code)
// })

// setTimeout(() => {
//   console.log('postMessage!!!!!!!!!!!')
//   worker.postMessage(pages)
// }, 2000);

///////////////////////////////////////////////////////////

// Event bus for subscription/dispatch

function EventBus() {
  const subscriptions = { }
  let nextId = 0
  function subscribe(eventType, callback) {
      const id = nextId
      nextId++

      if(!subscriptions[eventType])
          subscriptions[eventType] = { }

      subscriptions[eventType][id] = callback

      return {
          unsubscribe: () => {
              delete subscriptions[eventType][id]
              if (Object.keys(subscriptions[eventType]).length === 0) delete subscriptions[eventType]
          }
      }
  }

  function publish(eventType, arg) {
      if(!subscriptions[eventType])
          return

      Object.keys(subscriptions[eventType]).forEach(key => subscriptions[eventType][key](arg))
  }

  return { subscribe, publish }
}

function eventBusExample() {
  const bus = EventBus()
  const onRenderPage = bus.subscribe('onRenderPage', arg => console.log('onRenderPage', arg))

  bus.publish('onRenderPage', 'ok!!!!')
  onRenderPage.unsubscribe()
  bus.publish('onRenderPage', 'ok????')
}

// eventBusExample()


//////////////////////////////////////////////
