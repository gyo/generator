const schema = require('./schema')

module.exports = {
  output: 'build',
  clean: false,
  schema: [schema.Page],
  defaultLocale: 'en',
  locales: [
    {
      id: 'en',
      iso: 'en-US',
    },
    {
      id: 'es',
      iso: 'es-ES',
    },
    // {
    //   id: 'it',
    //   iso: 'it-IT',
    // },
  ],
  // indexBatch: 2,
  // renderBatch: 2
}
