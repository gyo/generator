const Generator = require('./generator')
const config = require('./config')
const react = require('./plugins/react')
const archives = require('./plugins/archives')
const watch = require('./plugins/watch')
const cluster = require('./plugins/cluster')

const generator = new Generator(Object.assign(config, {
  clean: false,
  useDB: false,
  renderOnStart: false
}))

generator.use(cluster({ isCluster: true }))
// generator.use(alternates)
generator.use(react())
// generator.use(archives())
// generator.use(hyperapp())
// generator.use(watch())
// generator.use(debug())
// generator.use(server())
generator.build()