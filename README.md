## Generator
A fast static-site generator for large scale projects.
Sources provide pages with data that's used by Transformers to modify and render them.

## TODO

[ ] Change the way alternate language versions are defined. Use the same `id` instead of
passing the translations all together with the default. The `route` is the unique field.

[ ] Add logic for when `useDB: false` so that pages are not indexed but rendered directly. (?)

[-] Add some way to split the pages rendering among several computers

[ ] Store a pointer to this.pages and other types (?)

[ ] Add Markdown plugin (which means adding support for filesystem sources)

[ ] Add support for domain-based routing for multi lingual websites

[ ] Add locales configuration with support for language fallbacks (en-us >> en, en-es >> en)

[x] Create a way to match pages inside plugins to avoid parsing undesired items

# Ideas
Grid computing logic:
- Split the pages in batches for the grid servers
- Zip and send each pages sub-batch to the grid servers
- Wait for the servers to send rendered pages and write them to disk