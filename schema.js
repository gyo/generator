// https://realm.io/docs/javascript/latest/api/tutorial-query-language.html

/*
Page
*/
module.exports.Page = {
  name: 'Page',
  schemaVersion: 1,
  primaryKey: 'id',
  properties: {
    id: 'string',
    route: { type: 'string?', indexed: true },
    locale: 'string',
    title: 'string?',
    content: 'string?',
    meta: 'string?',
    // query: 'string?[]',
    // something: { type: 'string', default: '', indexed: true}
  }
}

// const schemas = [
//   { schema: schema1, schemaVersion: 1, migration: migrationFunction1 },
//   { schema: schema2, schemaVersion: 2, migration: migrationFunction2 },
// ]

// // The first schema to update to is the current schema version
// // since the first schema in our array is at nextSchemaIndex:
// let nextSchemaIndex = Realm.schemaVersion(Realm.defaultPath);

// // If Realm.schemaVersion() returned -1, it means this is a new Realm file
// // so no migration is needed.
// if (nextSchemaIndex !== -1) {
//   while (nextSchemaIndex < schemas.length) {
//     const migratedRealm = new Realm(schemas[nextSchemaIndex++]);
//     migratedRealm.close();
//   }
// }

// // Open the Realm with the latest schema
// Realm.open(schemas[schemas.length-1]);