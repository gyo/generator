const regeneratorRuntime = require("regenerator-runtime")
const fs = require('fs')
const path = require('path')
const Realm = require('realm')

/**
 * Generator for large-scale static sites
 *
 * @param {object} config
 */
function Generator(config) {
  if (!(this instanceof Generator)) return new Generator(config)

  this.config = Object.assign({}, {
    // mode: 'development',
    mode: 'production',
    output: 'build',
    encoding: 'utf8',
    defaultLocale: 'en',
    locales: [],
    schema: [],
    plugins: [],
    indexBatch: 5000,
    renderBatch: 1000,
    temp: 'temp',
    db: 'db.realm',
    cleanBuild: true,
    cleanTemp: true,
    useDB: true,
    renderOnStart: true
  }, config)

  this.plugins = this.config.plugins || []
  this.firstRun = true
  this.profiler = Profiler()
  this.db = null
  this.schemaVersion = fs.existsSync(path.join(this.config.temp, this.config.db))
    ? Realm.schemaVersion(path.join(this.config.temp, this.config.db)) : 0
}

/**
 * Add a plugin to the transform pipeline
 *
 * @param {object} plugin
 */
Generator.prototype.use = function (plugin) {
  this.plugins.push(plugin)
  return this
}

/**
 * Apply plugins for a specific hook
 *
 * @param {string} hook The hook name
 * @param {array} args The arguments that will be passed to the hook functions
 */
Generator.prototype.hook = async function (hook, args) {
  for await (let plugin of this.plugins) {
    if (plugin[hook]) {
      if (args) { args[0] = await plugin[hook].apply(this, args) }
      else { await plugin[hook].apply(this, args) }
    }
  }
  return args ? args[0] : null
}

/**
 * Store pages in the DB
 *
 * @param {array} pages
 */
Generator.prototype.createNodes = function (pages = []) {
  // console.log('createNodes', pages);
  return new Promise(async resolve => {
    pages = await Promise.all(pages.map(page => this.normalizePage(page)))
    this.db.write(() => {
      for (let i = 0; i < pages.length; i++) {
        pages[i].forEach(p => !p.skipIndex && this.db.create('Page', p, true))
      }
      resolve()
    })
  })
}

/**
 * Render a page
 *
 * @param {object} page The page object
 * @param {boolean} write If true, the file will be created
 */
Generator.prototype.renderPage = async function (page, write = true) {
  // page = page._realm ? await this.parsePage(page) : page
  page = await this.hook('beforeRenderPage', [page])
  if (!page) return page
  page = await this.hook('onRenderPage', [page])
  if (write) await this.write(page)
  return page
}

/**
 * Build pages from DB
 *
 */
Generator.prototype.renderPages = async function (write = true) {
  this.profiler.start('renderPages')
  const pages = this.db.objects('Page')
  const batchTotal = Math.ceil(pages.length / this.config.renderBatch)
  let promises = []
  let batch = []

  const renderPendingBatches = async () => {
    if (!promises.length) return
    clearInterval(checkInterval)
    const batches = await Promises.all(promises)
    for (let i = 0; i < batches.length; i++) {
      for (let j = 0; j < batches[i].length; j++) {
        await this.write(batches[i][j])
      }
    }
    promises = []
    checkInterval = setInterval(renderPendingBatches, 10)
  }

  let checkInterval = !write ? false : setInterval(renderPendingBatches, 10)

  for (let i = 1; i <= batchTotal; i++) {
    let batch = pages.slice((i - 1) * this.config.renderBatch, i * this.config.renderBatch)
    for (let j = 0; j < batch.length; j++) {
      batch[j] = await this.parsePage(batch[j])
    }
    batch = await this.hook('onRenderBatch', [batch])

    // The batch is a promise, add it to the promises array
    if (batch.then && write) {
      promises.push(batch)
    // The batch was rendered, just write the pages
    } else if (batch.isRendered && write) {
      for (let j = 0; j < batch.length; j++) {
        await this.write(batch[j])
      }
    // The batch still needs to be rendered
    } else {
      for (let j = 0; j < batch.length; j++) {
        await this.renderPage(batch[j], write);
      }
    }
  }

  // Render remaining promises
  clearInterval(checkInterval)
  if (promises.length && write) {
    const batches = await Promises.all(promises)
    for (let i = 0; i < batches.length; i++) {
      for (let j = 0; j < batches[i].length; j++) {
        await this.write(batches[i][j])
      }
    }
  }

  this.afterRenderPages()
}

Generator.prototype.afterRenderPages = async function () {
  if (this.firstRun) {
    this.profiler.stop('renderPages')
    this.profiler.stop('total')
    await this.hook('ready')
    this.firstRun = false
  }
  console.log('Build done!', this.profiler.stats())
  if (this.config.mode === 'production') process.exit()
}

/**
 * Normalize the page object
 *
 * @param {object} page
 */
Generator.prototype.normalizePage = async function (page) {
  let nPage = Array.isArray(page) ? page : [page]
  const defaultPage = nPage.find(p => p.locale === this.config.defaultLocale) || {}
  nPage = nPage
    // Only leave pages with locales that are in use (but leave pages with undefined locale, assumed as default locale)
    .filter(p => this.config.locales.find(locale => !p.locale || p.locale === locale.id))
    // Normalize pages with defaults
    .map(p => {
      Object.keys(defaultPage).forEach(k => {
        if (p[k] === undefined) p[k] = defaultPage[k]
      })
      p.locale = p.locale || this.config.defaultLocale
      p.id = !p.id || (p.id === defaultPage.id && p.locale !== defaultPage.locale)
        ? defaultPage.id + '_' + p.locale
        : p.id.toString()
      p.route = p.route === 'index' ? '' : p.route
      p.route = p.locale === this.config.defaultLocale ? p.route : p.locale + (p.route ? '/' + p.route : '')
      return p
    })
  nPage = await this.hook('onCreateNode', [nPage])
  return nPage
}

/**
 * Parse a page coming from the DB
 * TODO: Remove the slice() and just pass a pointer to the view to avoid unnecessary computation (?)
 */
Generator.prototype.parsePage = async function (page) {
  page = await this.hook('onParsePage', [page])
  return this.config.schema.reduce((p, schema) => {
    if (schema.name === 'Page') {
      Object.keys(schema.properties).forEach(property => {
        p[property] = page[property] && page[property].forEach
          ? page[property].slice(0)
          : page[property] // unwrap
        // p[property] = page[property] // pointer
      })
    }
    return p
  }, {})
}

/**
 * Write a page to file (or other files in the future)
 */
Generator.prototype.write = async function (page) {
  this.profiler.start('Write')
  page = await this.hook('onWrite', [page])
  // In development mode only write the index in the root (other routes are rendered from the db on-demand)
  if (this.config.mode === 'production' ||
     (this.config.mode === 'development' && ['', '/', 'index', this.config.defaultLocale].indexOf(page.route) > -1)) {
    let route = path.join(this.config.output, page.route)
    await fs.promises.mkdir(route, { recursive: true })
    await fs.promises.writeFile(path.join(route, 'index.html'), page.content, this.config.encoding)
  }
  this.profiler.stop('Write')
}

/**
 * Run the build process
 */
Generator.prototype.build = async function () {
  // console.time('Build time')
  this.profiler.start('total')
  console.log('Initialisation...')
  await this.hook('init')

  ;(this.config.cleanBuild || this.config.cleanTemp) && this.profiler.start('cleanup')
  if (this.config.cleanBuild) {
    await fs.promises.rmdir(this.config.output, { recursive: true })
  }
  if (this.config.cleanTemp) {
    await fs.promises.rmdir(this.config.temp, { recursive: true })
  }
  if (!fs.existsSync(this.config.temp)) fs.mkdirSync(this.config.temp)
  ;(this.config.cleanBuild || this.config.cleanTemp) && this.profiler.stop('cleanup')

  if (this.config.useDB) {
    this.profiler.start('dbInit')

    const realmConfig = {
      path: path.join(this.config.temp, this.config.db),
      schema: this.config.schema,
      schemaVersion: this.schemaVersion,
      // deleteRealmIfMigrationNeeded: true,
    }

    try {
      this.db = new Realm(realmConfig)
    } catch (e) {
      if (e.message.indexOf('Migration') > -1) {
        this.schemaVersion += 1
        realmConfig.schemaVersion += 1
        this.db = new Realm(realmConfig)
      } else {
        console.log('Migration error:', e)
      }
    }

    this.profiler.stop('dbInit')

    // Index nodes
    console.log('Indexing...')
    this.profiler.start('indexNodes')

    // Get the sources from plugins
    const sources = this.plugins.reduce((acc, cur) => {
      if (cur.createNodes) {
        acc.push(cur.createNodes.call(this))
      }
      return acc
    }, [])

    // Read the sources
    let batch = []
    for (let i = 0; i < sources.length; i++) {
      for await (let source of sources[i]) {
        if (batch.length >= this.config.indexBatch) {
          await this.createNodes(batch)
          batch = []
        }
        if (source) batch.push(source)
      }
    }

    // Index remaining data...
    if (batch.length) {
      this.profiler.start('index')
      await this.createNodes(batch)
      this.profiler.stop('index')
    }

    this.profiler.stop('indexNodes')
  }

  if (this.config.renderOnStart) {
    console.log('Rendering...')
    await this.renderPages(this.config.mode === 'production')
  }
  // this.profiler.stop('renderPages')

  // this.profiler.stop('Total')
  // await this.hook('ready')
  // console.log(this.profiler.stats())
  // if (this.config.mode === 'production') process.exit()
}

module.exports = Generator

const Profiler = () => {
  const profiles = {}
  function ms2MS(ms) {
    const minutes = Math.floor(ms / 60000);
    const seconds = ((ms % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
  }
  const start = profile => {
    profiles[profile] = profiles[profile] || { start: 0, ms: 0 }
    if (profiles[profile].start > 0) stop(profile)
    profiles[profile].start = Date.now()
  }
  const stop = profile => {
    if (profiles[profile]) {
      profiles[profile].ms += Date.now() - profiles[profile].start
      profiles[profile].time = ms2MS(profiles[profile].ms)
      profiles[profile].start = 0
    }
  }
  const stats = () => profiles
  return { start, stop, stats }
}
