import React from 'react';
// import { Link } from "wouter";

// import { pullRight, h1 } from './layout.css';

const Layout = ({ children }) => {
  return (
    <div>
      <a href="/">
        Go to Home!
      </a>
      {children}
    </div>
  );
};

export default Layout;
