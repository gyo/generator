import React from 'react';
import Layout from './Layout';
import { Link } from '../context'

const DynamicPage = () => {
  return (
    <Layout>
      <h2>Dynamic Page!</h2>
      <p>This page was loaded asynchronously!!!</p>
      <Link to="/">Go to Home</Link>
    </Layout>
  );
};

export default DynamicPage;
