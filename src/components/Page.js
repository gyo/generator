import React, { useContext, useEffect } from 'react'
// import { Link } from "wouter"
import { Link } from '../context'

import State from '../context'
import Layout from './Layout'

const someAction = (e, state, setState) => {
  e.preventDefault()
  setState(state => ({ ...state, rand: Math.random() }))
}

const Home = (props) => {
  const [state, setState] = useContext(State)
  // console.log('Rendering', state)
  // useEffect(() => {
  //   setState(state => ({ ...state, address: "dsfsdfsdfsdf" }))
  //   console.log('after', state)
  // }, [])

  return (
    <Layout>
      <h1>Page Template</h1>
      <pre>{JSON.stringify(props, null, 2)}</pre>
      <Link to="/">Navigate to Home</Link>
    </Layout>
  )
}

export default Home
