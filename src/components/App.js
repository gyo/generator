import React, { useState, useContext } from 'react'
// import { Route, Switch } from "wouter"
import importedComponent from 'react-imported-component'

import State, { Router, Route, DynRoute, Link, History, GlobalProvider, useGlobal } from '../context'
import Home from './Home'
import Page from './Page'
import Loading from './Loading'
// if (typeof window !== 'undefined') window.History = History
// import "../index.css"

const AsyncDynamicPage = importedComponent(
  () => import(/* webpackChunkName:'DynamicPage' */ './DynamicPage'),
  {
    LoadingComponent: Loading
  }
)
const AsyncNoMatch = importedComponent(
  () => import(/* webpackChunkName:'NoMatch' */ './NoMatch'),
  {
    LoadingComponent: Loading
  }
)

// const AppOld = () => {
//   const [state, setState] = useState({})
//   return (
//     <State.Provider value={[state, setState]}>
//       <Switch>
//         <Route exact path="/" component={Home} />
//         <Route exact path="/es" component={Home} />
//         <Route exact path="/dynamic" component={AsyncDynamicPage} />
//         <Route component={AsyncNoMatch} />
//       </Switch>
//     </State.Provider>
//   )
// }

function NotFound() {
  return (
    <div>
      <p>404 - Not Found</p>
      <Link to="/">Back to home</Link>
    </div>
  )
}

const App = ({ route, page }) => {
  const [state, setState] = useState({ route, page })
  // const defaultState = { route, page }
  return (
    <State.Provider value={[state, setState]}>
      {/* <GlobalProvider defaultState={state}> */}
        <AppContent />
      {/* </GlobalProvider> */}
    </State.Provider>
  )
}

const AppContent = () => {
  // const [state, dispatch] = useGlobal()
  const [state, setState] = useContext(State)
  return (
    <Router context={State} route={state.route} components={{Home, Page}} loading={Loading} NotFound={NotFound}>
      <pre>{JSON.stringify(state.route)}</pre>
      <h1>
        {state.route.path === '/' && 'HOME!!!!!'}
        {state.route.path === '/about' && 'ABOUT!!!!!'}
        {/* {state.route.path === '/rucula-and-cherry-salad' && 'rucula-and-cherry-salad!!!!!'} */}
      </h1>
      <Route path="/">
        <p>Home</p>
        <img src="https://www.yourtrainingedge.com/wp-content/uploads/2019/05/background-calm-clouds-747964-1068x674.jpg" />
        <ul>
          <li><Link to="/about">Go to about</Link></li>
          <li><Link to="/somepage">Go to somepage</Link></li>
          <li><Link to="/dynamic">Go to dynamic route</Link></li>
          <li><Link to="/unknown">Go to unknown route</Link></li>
          <li onClick={() => History.push('/about')}>
            Programmatically go to about
          </li>
        </ul>
      </Route>
      <Route path="/about">
        <p>About</p>
        <Link to="/">Go to home</Link>
      </Route>
      <Route exact path="/dynamic" component={AsyncDynamicPage} />
      <DynRoute page={state.page} />
    </Router>
  )
}

export default App
