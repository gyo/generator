import React, { useContext, useEffect } from 'react'
// import { Link } from "wouter"
import { Link } from '../context'

import State from '../context'
import Layout from './Layout'

const someAction = (e, state, setState) => {
  e.preventDefault()
  setState(state => ({ ...state, rand: Math.random() }))
}

const Home = (props) => {
  const [state, setState] = useContext(State)
  // console.log('Rendering', state)
  // useEffect(() => {
  //   setState(state => ({ ...state, address: "dsfsdfsdfsdf" }))
  //   console.log('after', state)
  // }, [])

  return (
    <Layout>
      <h1>The Real Home</h1>
      <pre>{JSON.stringify(props, null, 2)}</pre>
      <p>
        <a href="#" onClick={e => someAction(e, state, setState)}>Set state</a>:
        {state.rand}
      </p>
      <Link to="/dynamic">Navigate to Dynamic Page</Link>
    </Layout>
  )
}

export default Home
