import { hot } from "react-hot-loader/root"
import React from "react"
import ReactDOM from "react-dom"
import { rehydrateMarks } from 'react-imported-component'
import App from "./components/App"

import "./index.css"

const initialState = window.__INITIAL_STATE__ || {}
const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate

const render = (Component) =>
  renderMethod(<Component {...initialState} />, document.getElementById('root'))

rehydrateMarks().then(() => render(hot(App)))

if (module.hot) {
  module.hot.accept();
}
