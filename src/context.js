const request = typeof fetch !== 'undefined' ? fetch : require('node-fetch')
import React, { useState, useEffect, useContext } from 'react'
import { createBrowserHistory, createMemoryHistory } from "history"

// const TinyHistory = () => {
//   return {
//     push() {
//       history.pushState(state, '', url);
//       const popStateEvent = new PopStateEvent('popstate', { state: state })
//       dispatchEvent(popStateEvent)
//     }
//   }
// }

// State

const State = React.createContext([{}, () => {}])
export default State

const GlobalStateContext = React.createContext()
const GlobalDispatchContext = React.createContext()

export function actions(state, action) {
  switch (action.type) {
    case 'navigate': {
      return { ...state, route: { path: action.payload } }
    }

    case 'increment': {
      return { count: state.count + 1 }
    }

    case 'decrement': {
      return { count: state.count - 1 }
    }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

export function GlobalProvider({children, defaultState}) {
  const [state, dispatch] = React.useReducer(actions, defaultState)
  return (
    <GlobalStateContext.Provider value={state}>
      <GlobalDispatchContext.Provider value={dispatch}>
        {children}
      </GlobalDispatchContext.Provider>
    </GlobalStateContext.Provider>
  )
}

export function useGlobalState() {
  const context = React.useContext(GlobalStateContext)
  if (context === undefined) {
    throw new Error('useGlobalState must be used within a GlobalProvider')
  }
  return context
}

export function useGlobalDispatch() {
  const context = React.useContext(GlobalDispatchContext)
  if (context === undefined) {
    throw new Error('useGlobalDispatch must be used within a GlobalProvider')
  }
  return context
}


export function useGlobal() {
  return [useGlobalState(), useGlobalDispatch()]
}



// Router

export function locationToRoute(location) {
  // location comes from the history package
  return {
    path: location.pathname,
    hash: location.hash,
    // query: qs.parse(location.search),
  }
}

// Example: "[Function: DynRoute]" => DynRoute
// TODO: change to regex
const parseFnSignature = signature => {
  let s = signature.split('[Function: ')
  return s[1]
    ? s[1].slice(0, -1).trim()
    : null
}

export const History = typeof document !== 'undefined' ? createBrowserHistory() : createMemoryHistory()

export const RouterContext = React.createContext({
  route: locationToRoute(History.location),
  component: null,
  data: null,
})

export function Route({ path, component, children }) {
  // Extract route from RouterContext
  const { route } = useContext(RouterContext)
  // console.log('Route', route, path, route.path === path)
  // Return null if the supplied path doesn't match the current route path
  if (route.path !== path) {
    return null
  }
  return component ? React.createElement(component, {}, children) : children
}

export function DynRoute() {
  const { route, component, data } = useContext(RouterContext)
  // console.log('DynRoute', component, data, route, data.route === route.path)
  // console.log('===================================')
  return component && '/' + data.route === route.path
    ? React.createElement(component, data)
    : null
}

export function Link(props) {
  const { to, onClick, children } = props
  // Extract route from RouterContext
  const { route } = useContext(RouterContext)
  const handleClick = (e) => {
    e.preventDefault()
    // console.log('Link', route.path, to)
    // Dont' navigate if current path
    if (route.path === to) return
    // Trigger onClick prop manually
    if (onClick) onClick(e)
    // Use history API to navigate page
    // console.log('History.push', to)
    History.push(to)
  }
  return (
    <a {...props} onClick={handleClick}>
      {children}
    </a>
  )
}

const handleRouteChange = (location, state, setState) => {
  console.log('handleRouteChange', location, state)
  fetch(location.location.pathname + (location.location.pathname === '/' ? '' : '/') + 'page-data.json')
    .then(data => {
      // console.log(data)
      return data.ok ? data.json() : undefined
    })
    .then(data => {
      // console.log('setState', data)
      const newState = { ...state, route: locationToRoute(location.location) }
      newState.page = data || newState.page
      setState(newState)
    })
    .catch(e => console.log('ERROR!!', e))

  // Fake request to location.pathname
  // if (location.location.pathname === '/') {
  //   // console.log('Fetching data for route:', location.location.pathname)
  //   setTimeout(() => {
  //     const fakeRouteResponse = {
  //       component: 'Home',
  //       data: {
  //         title: 'Some Title!!!',
  //       }
  //     }
  //     setState({ ...state, route: locationToRoute(location.location), page: fakeRouteResponse })
  //     // dispatch({ type: 'navigate', payload: {
  //     //   route: locationToRoute(location),
  //     //   page: fakeRouteResponse,
  //     // }})
  //   }, 1000)
  // } else {
  //   setState({ ...state, route: locationToRoute(location.location), page: {} })
  //   // dispatch({ type: 'navigate', payload: {
  //   //   route: locationToRoute(location),
  //   //   page: {},
  //   // }})
  // }
}

export const Router = props => {
  // console.log('Router', props)
  // TODO: Get the state/setState from the props or fallback to local state
  // OR maybe not... if the current route can be accessed through RouterContext
  const { children, context, route, components, NotFound } = props
  // console.log(children ? children.filter(c => parseFnSignature(c.type)) : null)
  // console.log(children.map(c => typeof c.type))

  const [state, setState] = context ? useContext(context) : useState({ route: route || { path: '/' } })
  // const [state, dispatch] = props.context ? props.context() : useState({ route: { path: '/' }, dyn: {} })
  // const [state, setState] = useState({ route: route || { path: '/' }, dyn: {} })
  // console.log('Router', state)
  const routerContextValue = {
    route: state.route,
    component: state.page ? components[state.page.component] : null,
    data: state.page || null,
  }

  useEffect(() => {
    const unlisten = History.listen(location => handleRouteChange(location, state, setState))
    return unlisten
  }, [])

  // Check if 404 if no route matched
  // const is404 = this.routes.indexOf(route.path) === -1
  return (
    <RouterContext.Provider value={routerContextValue}>
      {/* {is404 ? <NotFound/> : children} */}
      {children}
    </RouterContext.Provider>
  )
}

export class RouterOld extends React.Component {
  constructor(props) {
    super(props)
    // Convert our routes into an array for easy 404 checking
    // this.routes = Object.keys(props.routes).map((key) => props.routes[key].path)
    // Listen for path changes from the history API
    this.unlisten = History.listen(this.handleRouteChange)
    // Define the initial RouterContext value
    this.state = {
      route: locationToRoute(History.location),
    }
  }

  componentWillUnmount() {
    // Stop listening for changes if the Router component unmounts
    this.unlisten()
  }

  handleRouteChange = (location) => {
    const route = locationToRoute(location)
    this.setState({ route: route })
  }

  render() {
    // Define our variables
    const { children, NotFound } = this.props
    const { route } = this.state
    // Create our RouterContext value
    const routerContextValue = { route }
    // Check if 404 if no route matched
    // const is404 = this.routes.indexOf(route.path) === -1
    return (
      <RouterContext.Provider value={routerContextValue}>
        {is404 ? <NotFound/> : children}
      </RouterContext.Provider>
    )
  }
}
